#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#define RESISTOR_COLOR_BLACK  0  // B
#define RESISTOR_COLOR_BROWN  1  // b
#define RESISTOR_COLOR_RED    2  // r R
#define RESISTOR_COLOR_ORANGE 3  // o O
#define RESISTOR_COLOR_YELLOW 4  // y Y
#define RESISTOR_COLOR_GREEN  5  // g
#define RESISTOR_COLOR_BLUE   6  // l L
#define RESISTOR_COLOR_VIOLET 7  // v V
#define RESISTOR_COLOR_GRAY   8  // G
#define RESISTOR_COLOR_WHITE  9  // w W
#define RESISTOR_COLOR_GOLD   10 // d D
#define RESISTOR_COLOR_SILVER 11 // s S
#define RESISTOR_COLOR_NONE   12 // n N

static int resistor_parse_color(char *s, int *zeros, int *tolerance);
static int resistor_parse_value(char *s, int *zeros, int *tolerance);
static void print_color(int color_active, int color);
static void print_value(int value, int zeros, int base, char *unit);
static void print_code(int color_active, int value, int zeros, int tolerance);

int main(int argc, char *argv[]){
	int value, zeros, tolerance, color_active, opt;
	color_active = 1;
	while((opt = getopt(argc, argv, "bh")) != -1){
		if(opt == 'b'){
			color_active = 0;
		}else if(opt == 'h'){
			printf("Resistor Calculator, Rebane 2012\n");
			printf("Use: %s [-b] string [string] ...\n", argv[0]);
			printf("   -b - output without ANSI colors\n");
			printf("colors:\n");
			printf("   B   - (B)lack\n");
			printf("   b   - (b)rown\n");
			printf("   r R - (R)ed\n");
			printf("   o O - (O)range\n");
			printf("   y Y - (Y)ellow\n");
			printf("   g   - (g)reen\n");
			printf("   l L - b(L)ue\n");
			printf("   v V - (V)iolet\n");
			printf("   G   - (G)ray\n");
			printf("   w W - (W)hite\n");
			printf("   d D - gol(D)\n");
			printf("   s S - (S)ilver\n");
			printf("   n N - (N)one\n");
			printf("examples:\n");
			printf("   %s 1k5\n", argv[0]);
			printf("   %s 0.1\n", argv[0]);
			printf("   %s 5.5M\n", argv[0]);
			printf("   %s bByd\n", argv[0]);
			printf("   %s bggd 5K 1m1\n", argv[0]);
			return(0);
		}
	}
	if(argv[optind] == NULL){
		fprintf(stderr, "invalid command line, use %s -h for help\n", argv[0]);
		return(1);
	}
	opt = 0;
	while(argv[optind] != NULL){
		if(opt != 0)printf("====================\n");
		if((argv[optind][0] == '.') || (argv[optind][0] == ',') || isdigit(argv[optind][0]) || (argv[optind][0] == 'k') || (argv[optind][0] == 'K') || (argv[optind][0] == 'm') || (argv[optind][0] == 'M')){
			value = resistor_parse_value(argv[optind], &zeros, &tolerance);
			opt = 1;
		}else{
			value = resistor_parse_color(argv[optind], &zeros, &tolerance);
			opt = 2;
		}
		print_value(value, zeros, 3, "Ω");
		print_value(value, zeros, 6, "KΩ");
		print_value(value, zeros, 9, "MΩ");
		if(tolerance)printf("Tolerance: +/-%d%%\n", tolerance);

		print_code(color_active, value, zeros, tolerance);
		if((opt == 1) && (zeros > 1) && (value > 0) && (value < 10))print_code(color_active, value * 10, zeros - 1, tolerance);
		optind++;
	}
	return(0);
}

static int resistor_parse_value(char *s, int *zeros, int *tolerance){
	int i, j;
	char *e;
	*zeros = 3;
	*tolerance = 0;
	for(i = 0, j = 0; isdigit(*s); j++, s++){
		if(j < 2){
			i *= 10;
			i += (*s - '0');
		}else{
			(*zeros)++;
		}
	}
	j = 0;
	if((*s == '.') || (*s == ',')){
		if(i == 0){
			for( ; s[1] == '0'; s++)(*zeros)--;
			if(isdigit(s[1]) && isdigit(s[2])){
				(*zeros) -= 2;
				i = (((s[1] - '0') * 10) + (s[2] - '0'));
			}else if(isdigit(s[1])){
				(*zeros) -= 1;
				i = (s[1] - '0');
			}
		}else if(i < 10){
			if(isdigit(s[1])){
				(*zeros) -= 1;
				i = (i * 10) + (s[1] - '0');
			}
		}
		for(s++; isdigit(*s); s++);
	}
	if((*s == 'k') || (*s == 'K')){
		*zeros += 3;
		j = 1;
	}else if((*s == 'm') || (*s == 'M')){
		*zeros += 6;
		j = 1;
	}
	if(j){
		if(i == 0){
			if(isdigit(s[1]) && isdigit(s[2])){
				(*zeros) -= 2;
				i = (((s[1] - '0') * 10) + (s[2] - '0'));
			}else if(isdigit(s[1])){
				(*zeros) -= 1;
				i = (s[1] - '0');
			}
		}else if(i < 10){
			if(isdigit(s[1])){
				(*zeros) -= 1;
				i = ((i * 10) + (s[1] - '0'));
			}
		}
	}
	for( ; i && ((i >= 100) || !(i % 10)); (*zeros)++)i /= 10;
	if(*zeros < 1)i = 0;
	if(i == 0)*zeros = 0;
	if((i < 10) && (*zeros > 12)){
		i *= 10;
		(*zeros)--;
	}
	if(*zeros > 12)*zeros = 12;
	return(i);
}

static int resistor_parse_color(char *s, int *zeros, int *tolerance){
	int value;
	value = 0;
	*zeros = 0;
	*tolerance = 0;
	if(s[0] == 'b'){
		value = 10;
	}else if((s[0] == 'r') || (s[0] == 'R')){
		value = 20;
	}else if((s[0] == 'o') || (s[0] == 'O')){
		value = 30;
	}else if((s[0] == 'y') || (s[0] == 'Y')){
		value = 40;
	}else if(s[0] == 'g'){
		value = 50;
	}else if((s[0] == 'l') || (s[0] == 'L')){
		value = 60;
	}else if((s[0] == 'v') || (s[0] == 'V')){
		value = 70;
	}else if(s[0] == 'G'){
		value = 80;
	}else if((s[0] == 'w') || (s[0] == 'W')){
		value = 90;
	}
	if(strlen(s) > 1){
		if(s[1] == 'b'){
			value += 1;
		}else if((s[1] == 'r') || (s[1] == 'R')){
			value += 2;
		}else if((s[1] == 'o') || (s[1] == 'O')){
			value += 3;
		}else if((s[1] == 'y') || (s[1] == 'Y')){
			value += 4;
		}else if(s[1] == 'g'){
			value += 5;
		}else if((s[1] == 'l') || (s[1] == 'L')){
			value += 6;
		}else if((s[1] == 'v') || (s[1] == 'V')){
			value += 7;
		}else if(s[1] == 'G'){
			value += 8;
		}else if((s[1] == 'w') || (s[1] == 'W')){
			value += 9;
		}
	}
	if(strlen(s) > 2){
		if(s[2] == 'B'){
			*zeros = 3;
		}else if(s[2] == 'b'){
			*zeros = 4;
		}else if((s[2] == 'r') || (s[2] == 'R')){
			*zeros = 5;
		}else if((s[2] == 'o') || (s[2] == 'O')){
			*zeros = 6;
		}else if((s[2] == 'y') || (s[2] == 'Y')){
			*zeros = 7;
		}else if(s[2] == 'g'){
			*zeros = 8;
		}else if((s[2] == 'l') || (s[2] == 'L')){
			*zeros = 9;
		}else if((s[2] == 'v') || (s[2] == 'V')){
			*zeros = 10;
		}else if(s[2] == 'G'){
			*zeros = 11;
		}else if((s[2] == 'w') || (s[2] == 'W')){
			*zeros = 12;
		}else if((s[2] == 'd') || (s[2] == 'D')){
			*zeros = 2;
		}else if((s[2] == 's') || (s[2] == 'S')){
			*zeros = 1;
		}
	}
	if(strlen(s) > 3){
		if((s[3] == 'd') || (s[3] == 'D')){
			*tolerance = 5;
		}else if((s[3] == 's') || (s[3] == 'S')){
			*tolerance = 10;
		}else if((s[3] == 'n') || (s[3] == 'N')){
			*tolerance = 20;
		}
	}
	return(value);
}

static void print_color(int color_active, int color){
	if(color == RESISTOR_COLOR_BLACK){
		if(color_active)printf("\x1B[47m\x1B[30m");
		printf("Black");
	}else if(color == RESISTOR_COLOR_BROWN){
		if(color_active)printf("\x1B[33m");
		printf("Brown");
	}else if(color == RESISTOR_COLOR_RED){
		if(color_active)printf("\x1B[31m");
		printf("Red");
	}else if(color == RESISTOR_COLOR_ORANGE){
		if(color_active)printf("\x1B[1m\x1B[31m");
		printf("Orange");
	}else if(color == RESISTOR_COLOR_YELLOW){
		if(color_active)printf("\x1B[1m\x1B[33m");
		printf("Yellow");
	}else if(color == RESISTOR_COLOR_GREEN){
		if(color_active)printf("\x1B[32m");
		printf("Green");
	}else if(color == RESISTOR_COLOR_BLUE){
		if(color_active)printf("\x1B[34m");
		printf("Blue");
	}else if(color == RESISTOR_COLOR_VIOLET){
		if(color_active)printf("\x1B[35m");
		printf("Violet");
	}else if(color == RESISTOR_COLOR_GRAY){
		if(color_active)printf("\x1B[1m\x1B[30m");
		printf("Gray");
	}else if(color == RESISTOR_COLOR_WHITE){
		if(color_active)printf("\x1B[1m\x1B[37m");
		printf("White");
	}else if(color == RESISTOR_COLOR_GOLD){
		if(color_active)printf("\x1B[1m\x1B[33m");
		printf("Gold");
	}else if(color == RESISTOR_COLOR_SILVER){
		if(color_active)printf("\x1B[1m\x1B[30m");
		printf("Silver");
	}else if(color == RESISTOR_COLOR_NONE){
		printf("None");
	}
	if(color_active)printf("\x1B[0m");
}

static void print_value(int value, int zeros, int base, char *unit){
	int i;
	printf("Value: ");
	if(value == 0){
		printf("0");
	}else{
		if(zeros < base){
			if((zeros == (base - 1)) && (value > 9)){
				printf("%d.%d", (value / 10), (value % 10));
			}else{
				printf("0.");
				if(value > 9)zeros++;
				for(i = 0; i < (base - zeros - 1); i++)printf("0");
				printf("%d", value);
			}
		}else{
			printf("%d", value);
			for(i = 0; i < (zeros - base); i++)printf("0");
		}
	}
	printf(" %s\n", unit);
}

static void print_code(int color_active, int value, int zeros, int tolerance){
	print_color(color_active, value / 10);
	printf(" ");
	print_color(color_active, value % 10);
	printf(" ");
	if(zeros == 0){
		print_color(color_active, RESISTOR_COLOR_BLACK);
	}else if(zeros == 1){
		print_color(color_active, RESISTOR_COLOR_SILVER);
	}else if(zeros == 2){
		print_color(color_active, RESISTOR_COLOR_GOLD);
	}else{
		print_color(color_active, zeros - 3);
	}
	printf(" ");
	if(tolerance == 0){
		printf("(");
		print_color(color_active, RESISTOR_COLOR_GOLD);
		printf(" - +/-5%, ");
		print_color(color_active, RESISTOR_COLOR_SILVER);
		printf(" - +/-10%, ");
		print_color(color_active, RESISTOR_COLOR_NONE);
		printf(" - +/-20%)");
	}else if(tolerance == 5){
		print_color(color_active, RESISTOR_COLOR_GOLD);
	}else if(tolerance == 10){
		print_color(color_active, RESISTOR_COLOR_SILVER);
	}else if(tolerance == 10){
		print_color(color_active, RESISTOR_COLOR_NONE);
	}
	printf("\n");
}

